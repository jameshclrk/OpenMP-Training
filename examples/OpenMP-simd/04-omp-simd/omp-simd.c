#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

#define SIZE 100000000
#define INDEXES 8

void initialise(int *hist, int *b, int *c) {
  srand(1);
  for(int i = 0; i < SIZE; i++){
    int index1 = rand() % INDEXES;
    int index2 = rand() % INDEXES;
    b[i] = index1;
    c[i] = index2;
  }
  for(int i = 0; i < INDEXES*2; i++){
    hist[i] = 0;
  }
}

int main(){
  int hist[INDEXES*2], *b, *c;

  /* Allocate memory */
  b = _mm_malloc(sizeof(int)*SIZE, 64);
  c = _mm_malloc(sizeof(int)*SIZE, 64);

  double start, finish;

  /* Initialise arrays (basically a dice roll) */
  initialise(hist, b, c);

  /* Compute a histogram - e.g. histogram analysis of an image. */
  start = omp_get_wtime();
#pragma omp simd reduction(+:hist[:]) aligned(b,c: 64)
  for(int i = 0; i < SIZE; i++){
    int val = b[i] + c[i];
    hist[val]++;
  }
  finish = omp_get_wtime();

  printf("Runtime was %lfs.\n", finish-start);
  /* Output histogram */
  for(int i = 0; i < INDEXES*2; i++){
    printf("%i ", hist[i]);
  }
  printf("\n");

  /* Free memory */
  _mm_free(b);
  _mm_free(c);
}

