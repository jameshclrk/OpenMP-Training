import pandas
import matplotlib.pyplot as plt
import numpy as np

omp = pandas.read_csv('results/omp.time', names=['cores', 'time'], delimiter=' ')
task = pandas.read_csv('results/task.time', names=['cores', 'time'], delimiter = ' ')
depend = pandas.read_csv('results/depend.time', names=['cores', 'time'], delimiter = ' ')

ax = plt.subplot(2,2,1)
#ax = plt.gca()
omp.plot.line(x='cores',y='time',ax=ax,label='omp for')
task.plot.line(x='cores',y='time',ax=ax,label='omp task')
depend.plot.line(x='cores',y='time',ax=ax,label='omp task depend')
ax.set_xscale("log")
ax.set_yscale("log")
ax.set_ylabel('Time (s)')


ax = plt.subplot(2,2,2)
#ax = plt.gca()

list = omp['time'].tolist()
relative = list[0]
for i in range(len(list)-1,-1,-1):
  list[i] = relative / list[i]

ompScaling = pandas.DataFrame({'cores':omp['cores'].tolist(),'Speedup':list})

list =  task['time'].tolist()
relative = list[0]
for i in range(len(list)-1,-1,-1):
  list[i] = relative / list[i]

taskScaling = pandas.DataFrame({'cores':task['cores'].tolist(),'Speedup':list})

list =  depend['time'].tolist()
relative = list[0]
for i in range(len(list)-1,-1,-1):
  list[i] = relative / list[i]
  

dependScaling = pandas.DataFrame({'cores':depend['cores'].tolist(),'Speedup':list})

list = range(1,33)
Ideal = pandas.DataFrame({'cores':depend['cores'].tolist(), 'Speedup' : list})

ax.set_xlim(1,32)
ax.set_ylim(0.8,32)
ompScaling.plot.line(x='cores',y='Speedup', label='omp for',marker='o',ax=ax)
taskScaling.plot.line(x='cores',y='Speedup',ax=ax,marker='o', label='omp task')
dependScaling.plot.line(x='cores',y='Speedup',ax=ax, marker='o',label='omp task depend')
Ideal.plot.line(x='cores',y='Speedup',ax=ax, label='Ideal Speedup',linestyle=':',color='Black')


scaling = ompScaling['Speedup'].tolist()
for i in range(len(list)-1,-1,-1):
  scaling[i] = scaling[i]/list[i]

ompEfficiency = pandas.DataFrame({'cores':omp['cores'].tolist(),'Efficiency':scaling})

scaling = taskScaling['Speedup'].tolist()
for i in range(len(list)-1,-1,-1):
  scaling[i] = scaling[i]/list[i]

taskEfficiency = pandas.DataFrame({'cores':task['cores'].tolist(),'Efficiency':scaling})

scaling = dependScaling['Speedup'].tolist()
for i in range(len(list)-1,-1,-1):
  scaling[i] = scaling[i]/list[i]

dependEfficiency = pandas.DataFrame({'cores':depend['cores'].tolist(),'Efficiency':scaling})

list2 = np.ones(32)
Ideal2 = pandas.DataFrame({'cores':depend['cores'].tolist(),'Efficiency':list2})

ax = plt.subplot(2,2,3)
#ax = plt.gca()
ax.set_xlim(1,32)
ax.set_ylim(0.0,1.1)
ompEfficiency.plot.line(x='cores',y='Efficiency', label='omp for',marker='o',ax=ax)
taskEfficiency.plot.line(x='cores',y='Efficiency',label='omp task',marker='o',ax=ax)
dependEfficiency.plot.line(x='cores',y='Efficiency',label='omp task depend',marker='o', ax=ax)
Ideal2.plot.line(x='cores',y='Efficiency',ax=ax, label='Ideal', linestyle=':',color='Black')

plt.savefig("scaling.png")
plt.show()
