#define nparts 50000
#define nsteps 10
#define mass 1.0
#define dt 0.1
#define eps 1.0
#define sigma 0.01

const double box[3] = { 10.0, 10.0, 10.0 };

typedef struct data {
  double *restrict x_pos __attribute__((aligned(64))),
      *restrict y_pos __attribute__((aligned(64))),
      *restrict z_pos __attribute__((aligned(64))),
      *restrict x_vel __attribute__((aligned(64))),
      *restrict y_vel __attribute__((aligned(64))),
      *restrict z_vel __attribute__((aligned(64))),
      *restrict x_force __attribute__((aligned(64))),
      *restrict y_force __attribute__((aligned(64))),
      *restrict z_force __attribute__((aligned(64))),
      *restrict x_accel __attribute__((aligned(64))),
      *restrict y_accel __attribute__((aligned(64))),
      *restrict z_accel __attribute__((aligned(64))),
      *restrict cutoff __attribute__((aligned(64)));

  double E0, e_pot, e_kin;
} data;
