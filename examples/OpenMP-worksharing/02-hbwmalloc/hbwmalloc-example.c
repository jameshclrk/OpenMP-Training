/* hbwmalloc-example.c
 *
 * This file contains some examples for allocating high bandwidth
 * memory in C. The functions used from the hbwmalloc library are:
 *
 *  (Text from the hbwmalloc man page)
 *
 *   hbw_check_available(void)
 *       returns 0 if high bandwidth memory is available and ENODEV if high 
 *       bandwidth memory is unavailable.
 *
 *   hbw_malloc(size_t size)
 *       allocates size bytes of uninitialized high bandwidth memory. The 
 *       allocated space is suitably aligned (after possible pointer coercion) 
 *       for storage of any type of object. If size is zero then hbw_malloc()
 *       returns NULL
 *
 *   hbw_verify_memory_region(void *addr, size_t size, int flags)
 *       verifies if memory region fully fall into high bandwidth memory.
 *       Returns: 0 if memory in address range from addr to addr + size is 
 *       allocated in high bandwidth memory, -1 if any fragment of memory was 
 *       not backed by high bandwidth memory [e.g. when memory is not 
 *       initalized] or EINVAL if addr is NULL, size equals 0 or flags 
 *       contained unsupported bit set. If memory pointed by addr could not be 
 *       verified then EFAULT is returned
 *
 *       Using this function in production code may result in serious
 *       performance penalty.
 *
 *       Flags argument may include optional flags that modifies function
 *       behaviour: 
 *         HBW_TOUCH_PAGES: Before checking pages, function will touch first
 *         byte of all pages in address range starting from addr to addr + size
 *          by read and write (so the content will be overwitten by the same 
 *          data as it was read). Using this option may trigger Out Of Memory
 *          killer.
 *
 *   hbw_free(void *ptr)
 *       causes the allocated memory referenced by ptr to be made available for
 *       future allocations. If ptr is NULL, no action occurs. The address ptr,
 *       if not NULL, must have been returned by a previous call to
 *       hbw_malloc(), hbw_calloc(), hbw_realloc(), hbw_posix_memalign(), or 
 *       hbw_posix_memalign_psize(). Otherwise, if hbw_free(ptr) was called 
 *       before, undefined behavior occurs.
 */

#include <stdio.h>
#include <assert.h>
#include <errno.h>

/* include the hbwmalloc header */
#include <hbwmalloc.h>

/* Memory size in bytes */
#define MEM_SIZE  50000

int main (void) {
    int check_mem;
    void * ddr_memory, * hbw_memory;

    /* check that there is high bandwidth memory available */
    check_mem = hbw_check_available();
    if (check_mem == 0) {
        fprintf(stdout, "HBW memory available!\n");
    } else {
        fprintf(stderr, "hbw_check_available returned %d. No hbw memory available!\n", check_mem);
        return ENOMEM;
    }

    /* Allocate some traditional memory */
    ddr_memory = malloc(MEM_SIZE);
    /* Allocate some high bandwidth memory */
    hbw_memory = hbw_malloc(MEM_SIZE);

    /* Print pointers */
    printf("ddr_memory : %p\n", ddr_memory);
    printf("hbw_memory : %p\n", hbw_memory);

    /* Check if memory is allocated in a high bandwidth memory region */
    printf("Is ddr_memory in a hbw region? : %s\n", hbw_verify_memory_region(ddr_memory, MEM_SIZE, 0) == 0 ? "yes" : "no");
    printf("Id hbw_memory in a hbw_region? : %s\n", hbw_verify_memory_region(hbw_memory, MEM_SIZE, 0) == 0 ? "yes" : "no");

    /* Free some traditional memory */
    free(ddr_memory);
    /* Free some high bandwidth memory */
    hbw_free(hbw_memory);

    return 0;
}

