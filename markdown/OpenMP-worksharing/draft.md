
# Worksharing Exercises

## Setup
The initial code for these exercises is in the `examples/OpenMP-worksharing`. These exercises introduce the OpenMP `for` method for parallelism.

## Helloworld

- Go to `examples/OpenMP-worksharing/00\_helloworld`
- Compile `[icc/ifort] -Ofast -fopenmp helloworld.[c/F90] -o helloworld`
- Run locally


- Compile `[icc/ifort] -Ofast helloworld.[c/F90] -o helloworld`
- Run locally


- Compile `[icc/ifort] -Ofast -fopenmp helloworld\_rl.[c/F90] -o helloworld`
- Run locally
- Run on Skylake / KNL using lsf scripts. 

- Compile `[icc/ifort] -Ofast helloworld\_rl.[c/F90] -o helloworld`
- Run locally

## Memory

- Go to `examples/OpenMP-worksharing/01-numactl`
- Execute `make` to compile
- Try running the code on SKL and KNL MCDRAM and DDR4. What numbers do you see?
- Which number of threads gives the best performance?
- What sort of speedup do you see?
- Why is the performance slow on 1 thread?

- Go to `examples/OpenMP-worksharing/02-hbwmalloc`
- Execute `make` to compile
- What happens when you increase `MEM\_SIZE` to more than can fit in MCDRAM?


## Synchonization

## Schedulers

## Memory and Thread Placement


