Molecular Dynamics Exercises
==========================

## Setup
Open the `examples/OpenMP-tasks/md/` directory and load the Intel compiler with `module load intel`. Build the initial version with `make`.

##Exercise 1
Copy the md.c from `initial` to the `ex1` folder. Add OpenMP parallelism to the loops in the `compute_step` and `update` functions. Submit a job with the `efficiency.lsf` script and check the results - how much quicker was the code in parallel vs serial?

By default, OpenMP uses the `static` loop schedule. Add the `schedule(runtime)` clause to the loops in `ex1/md.c`.

Edit the submission script and try the `guided` and `dynamic` schedules, and test different chunksizes. Why do the different schedules and chunksizes affect the performance?

You can also test the same code on a KNL with 64, 128 and 256 threads and compare the performance.

##Exercise 2
Copy the md.c file from `Ex1` to `Ex2`. Alter your implementation from exercise 1 to use `taskloop` in `compute_step` (don't attempt to use reduction with `taskloop`).

Build this version with `make ex2` and compare the runtime to your previous versions. Does altering the `grain_size` (or number of tasks) affect performance?

If your new version is substantially slower, use `make ex2_opt` to build the optimisation report. You may find the code no longer vectorises. If you avoid updating the arrays inside the inner loop, and instead sum to a temporary value the compiler can vectorise the code.

##Exercise 3
Create a new folder (`ex3`) and copy `initial/md.c` to `ex3/md.c`. Break down the outer loop in the `compute_step` and create explicit tasks. Copy the code from exercise 1 to parallelise the update function.How does the performance compare to Exercise 2?

Build the new version with `make ex3` and compare the runtime. Parallelise the `update` function with explicit tasks too and see how that affects performance.

##Exercise 4
Copy the code from the third exercise into a new folder (`ex4`). Create dependencies between the tasks in `create_step` and `update_tasks`, and build the code with `make ex4`.
Ensure there are no OpenMP barriers between the start of `create_step` and the end of `update_tasks`. How does the performance compare?

If you have time, try to change the code from one-sided updates (i.e. only updating particle _i_) to updating both particles involved in an interaction.



