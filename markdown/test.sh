
mkdir -p build
pandoc OpenMP-simd/draft.md  -f markdown -t rst -s -o source/OpenMP-simd.rst
pandoc OpenMP-worksharing/draft.md  -f markdown -t rst -s -o source/OpenMP-worksharing.rst
pandoc OpenMP-tasks/GameofLife.md  -f markdown -t rst -s -o source/GameofLife.rst
pandoc OpenMP-tasks/MolecularDynamics.md -f markdown -t rst -s -o source/MolecularDynamics.rst
pandoc login.md -f markdown -t rst -s -o source/login.rst

cp ../presentations/OpenMP-*.pdf source/.

make html

rm source/OpenMP-simd.rst
rm source/OpenMP-worksharing.rst
rm source/GameofLife.rst
rm source/MolecularDynamics.rst
rm source/login.rst
rm source/OpenMP-*.pdf
