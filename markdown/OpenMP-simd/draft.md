
# Vectorisation Exercises


## novec

In `Vectorisation/01-novec`:

There is an example code with two identical loops, operating on identical data but one is annotated with `#pragma novector`

Compile the code with `make`

What does the optimisation report say about the two loops?

How much faster do you expect the code to run with the vectorised loop?

Now, run the compiled code.

Why doesn’t the code perform as well as expected?

What happens if you change the ARCH variable in the Makefile?

(Remember to `make clean` first)


## dependencies

This example generates a histogram, based on two input arrays.
This could be for image analysis, “binning” different statistics for output, etc.
The compiler will complain:
“loop was not vectorized: vector dependence prevents vectorization”
Then will tell you what the dependence is:
“remark #15346: vector dependence: assumed FLOW dependence  between histogram line 40 and histogram line 40
remark #15346: vector dependence: assumed ANTI dependence   between histogram line 40 and histogram line 40”

How to fix this kind of dependency?
Strip mining! Turns one loop in to multiple loops.
`02-dependencies` in the vectorisation example directory shows the code.

Typing `make` will compile the original code and the solution.

Try implement a solution yourself
Try changing the strip width. Does it change the performance?


Watch out for going out of bounds when accessing arrays!


## restrict

In the directory `Vectorisation/03-restrict`:
Run make then check “loop.optrpt”
What do you see? Why does the report show this?
Run `make RESTRICT=1` then check “loop.optrpt”
What has changed?
The important code is in “loop.c”


## OpenMP Simd

Look at the C code for the dependency example.

We had to strip mine, but it was only a reduction!

Take the original C code in `02-dependencies` and try add SIMD with a reduction.

A solution is in OpemMP/omp-simd:
`#pragma omp simd reduction(+:histogram[:]) aligned(b,c: 64)`


